#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psycopg2
import config
import pg_dump 
import solr_backup 
import resources_backup 
import storage_backup 
import subprocess
import datetime

def connect(section):
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config.ConfigDB(section)

        # connect to the PostgreSQL server
        print ('Connecting to the PostgreSQL database from ', section)
        conn = psycopg2.connect(**params)

        # create a cursor
        cur = conn.cursor()

        # execute a statement
        print ('PostgreSQL database version:')
        cur.execute('SELECT version()')

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

def get_databases(section):
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config.ConfigDB(section)
        result=[]
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
        # create a cursor
        cur = conn.cursor()
        # execute a statement
        print('Get databases:')
        cur.execute("""SELECT datname FROM pg_database
            WHERE datistemplate = false;""")
        # display the PostgreSQL database server version
        for table in cur.fetchall():
            result.append(table)
            # print(table)

		# close the communication with the PostgreSQL
        # cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        # if conn is not None:
            # conn.close()
            # print('Database connection closed.')
            return result

if __name__ == '__main__':
    #connect()
    sections = config.ReadConfig() 
    path_backup = config.ConfigSectionGet('listdb','path')
    host_name = config.ConfigSectionGet('db1', 'host')
    user_name = config.ConfigSectionGet('db1', 'user')
    password = config.ConfigSectionGet('db1', 'password')
    sshhost = config.ConfigSectionGet('ssh', 'sshhost')
    sshuser = config.ConfigSectionGet('ssh', 'sshuser')
    sshpassword = config.ConfigSectionGet('ssh', 'sshpassword')
    solrpath = config.ConfigSectionGet('ssh', 'solrpath')
    resourcespath = config.ConfigSectionGet('ssh', 'resourcespath')
    storagepath = config.ConfigSectionGet('ssh', 'storagepath')
  
    res = get_databases('db1')
    # print(res)

    # create folder for backup file
    dt = datetime.datetime.now()
    folder_name = dt.strftime("%Y%m%d-%H%M%S")
    cmd1 = 'mkdir ' +  path_backup.replace('\'','') + 'postgres/' + folder_name
    cmd2 = 'mkdir ' +  path_backup.replace('\'','') + 'solr/' + folder_name
    cmd3 = 'mkdir ' +  path_backup.replace('\'','') + 'resources/' + folder_name
    cmd4 = 'mkdir ' +  path_backup.replace('\'','') + 'storage/' + folder_name 

    # execute create folder
    popen = subprocess.Popen(cmd1, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    popen = subprocess.Popen(cmd2, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    popen = subprocess.Popen(cmd3, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    popen = subprocess.Popen(cmd4, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
  
    popen.stdout.close()
    popen.wait()

    # dump database postgre
    for r in res: 
    	database_name = r[0]
    	print ('Starting backup database: ' + database_name)
    	pg_dump.RunPG_dump(host_name, user_name, password, database_name, path_backup, folder_name)
    	print ('Backup complite')

    # solr zip copy backup
    solr_backup.RunSolr_Backup(sshhost, sshuser, sshpassword, solrpath, path_backup, folder_name)

    # resources zip copy backup
    resources_backup.RunResources_Backup(sshhost, sshuser, sshpassword, resourcespath, path_backup, folder_name)

    # storage zip copy backup
    storage_backup.RunStorage_Backup(sshhost, sshuser, sshpassword, storagepath, path_backup, folder_name)
 
 