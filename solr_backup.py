#!/usr/bin/python
# -*- coding: utf-8 -*-

import subprocess
import datetime

def RunSolr_Backup (host_name, user_name, password, solrpath, path_backup, folder_name):
      # dt = datetime.datetime.now()
      # date = dt.strftime("%Y-%m-%d-%H.%M.%S")

      
      cmd1 = 'sshpass -p ' + password + ' scp -r -o StrictHostKeyChecking=no -o PreferredAuthentications=password -o PubkeyAuthentication=no ' + user_name + '@' + host_name + ':' + solrpath.replace('\'','') + ' ' + path_backup.replace('\'','') + 'solr/' + folder_name + '/' + 'collection1 > ' + path_backup.replace('\'','') + 'solr/' + folder_name + '/finish-copy.txt'
      # cmd3 = "sleep 5; while [ ! -f " + path_backup.replace('\'','') + "solr/" + folder_name + "/finish-copy.txt ]; do printf '.'; sleep 1; done; printf 'finish'; " + ' zip -r '+ path_backup.replace('\'','') + 'solr/' + folder_name + '/' + 'collection1.zip ' + path_backup.replace('\'','') + 'solr/' + folder_name + '/' + 'collection1 > ' + path_backup.replace('\'','') + 'solr/' + folder_name + '/finish-zip.txt'
      
      print(cmd1)
      # print(cmd3)

      popen = subprocess.Popen(cmd1, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
      # popen = subprocess.Popen(cmd3, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)

      popen.stdout.close()
      popen.wait()
