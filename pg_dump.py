#!/usr/bin/python
# -*- coding: utf-8 -*-

import subprocess
import datetime

def RunPG_dump (host_name, user_name, password, database_name, path_backup, folder_name):
      dt = datetime.datetime.now()
      date = dt.strftime("%Y-%m-%d-%H.%M.%S")
  
      file_name = date + '-' + database_name + '.tar.gz' 
      # file_name = date + '-' + database_name + '.sql' 
 
      cmd = 'export PGPASSWORD='+ password +' && ' + 'pg_dump -O -h '+ host_name +' -U ' + user_name + ' ' + database_name + ' | gzip -9 > ' + path_backup.replace('\'','') + 'postgres/' + folder_name + '/' + file_name
      # cmd = 'export PGPASSWORD='+ password +' && ' + 'pg_dump -O -h '+ host_name +' -U ' + user_name + ' ' + database_name + ' > ' + path_backup.replace('\'','') + 'postgres/' + folder_name + '/' + file_name
      
      # print(cmd)
      
      popen = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
      popen.stdout.close()
      popen.wait()
